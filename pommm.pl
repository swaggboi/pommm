#!/usr/bin/env perl

# Daniel Bowling <swaggboi@slackware.uk>
# Aug 2020

use strict;
use warnings;
use utf8;
use open qw(:std :utf8); # Fix "Wide character in print" warning
use XML::LibXML;
use WebService::Mattermost;
#use Data::Dumper;        # Uncomment for debugging

## Variables ##

# Get creds file
my $dotfile =
    (-r "$ENV{HOME}/.mmCreds.xml") ? "$ENV{HOME}/.mmCreds.xml" :
    (-r ".mmCreds.xml")            ? ".mmCreds.xml"            :
    die "Dotfile not found!\n";
# Open the creds file
my $dom = XML::LibXML->load_xml(location => $dotfile);
# Grab the values
my $user = $dom->findnodes('/credentials/username')->to_literal();
my $pass = $dom->findnodes('/credentials/password')->to_literal();
my $chan = $dom->findnodes('/credentials/channel_id')->to_literal();
my $url  = $dom->findnodes('/credentials/base_url')->to_literal();
# Put the values into place
my %conf = (
    authenticate => 1,
    username     => "$user",
    password     => "$pass",
    base_url     => "$url"
    );

# Create new WebService::Mattermost objects (mm && resource)
my $mm       = WebService::Mattermost->new(%conf);
my $resource = $mm->api->posts;

## Begin ##

# Must specify full path for `pom`
chomp(my $pom = `/usr/games/pom`);

# Add emoji to match phase (markdown emphasis on Full or New)
$pom =
    /The Moon is New/                  ? "**$pom** 🌚" :
    /The Moon is Waxing Crescent/      ? "$pom 🌒"     :
    /The Moon is at the First Quarter/ ? "$pom 🌛"     :
    /The Moon is Waxing Gibbous/       ? "$pom 🌔"     :
    /The Moon is Full/                 ? "**$pom** 🌝" :
    /The Moon is Waning Gibbous/       ? "$pom 🌖"     :
    /The Moon is at the Last Quarter/  ? "$pom 🌜"     :
    /The Moon is Waning Crescent/      ? "$pom 🌘"     :
    $_ for $pom;

# Send it
$resource->create(
    {
        channel_id => "$chan",
        message    => "$pom"
    }
    );

# TODO: This breaks script?:
#my %post = (
#    channel_id => "$chan",
#    message    => 'testing 123...'
#    );
#print $resource->create(%post) . "\n";
# Output:
# Can't use string ("channel_id") as a HASH ref while "strict refs" in
# use at
# /home/daniel/perl5/lib/perl5/WebService/Mattermost/V4/API/Resource/Posts.pm
# line 14.
