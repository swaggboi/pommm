# pommm

Perl script I run via cron to post the phase of moon (using the pom
command) to a Mattermost channel

_pommm eq "Phase of Moon MatterMost";_

pommm wants to run alongside a dot file:

- $HOME/.mmCreds.xml or .mmCreds.xml in same directory

    The Mattermost credentials to use

This script relies on XML::LibXML, WebService::Mattermost (alongside
its many dependencies) and you'll need the `pom` command to be
available at `/usr/games/pom`.

## Credentials

- Base URL

    This will likely be https://%(your server's hostname)/api/v4/

- Username

    Can be the email you registed with in lieu of actual username

- Password

    You can't borrow mine, sorry

- Channel ID

    In the UI: Select the channel you want then click on the channel
    name in upper part of the screen, click "View Info" then look for
    the tiny nearly invisible letters on the bottom of the window you
    just opened

## The `pom` Command

The `pom` command typically comes bundled with a package called `bsdgames`:

### Slackware

    root@optepr0n:~/git_repos/pom_swagg# slackpkg file-search pom
    
    
    NOTICE: pkglist is older than 24h; you are encouraged to re-run 'slackpkg update'
    
    Looking for pom in package list. Please wait... /-\|DONE
    
    The list below shows the packages that contains "pom" file.
    
    [ Status           ] [ Repository               ] [ Package                                  ]
       installed               slackware64                  bsd-games-2.13-x86_64-12                  
    
    You can search specific packages using "slackpkg search package".

### Debian

    root@timecube:~# apt-file find /usr/games/pom
    bsdgames: /usr/games/pom
